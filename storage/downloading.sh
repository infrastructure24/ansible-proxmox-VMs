#! /bin/bash

repo='https://raw.githubusercontent.com/kubernetes-csi/csi-driver-smb/master/deploy/v1.15.0/'

curl -skSL "$repo/csi-smb-controller.yaml" > csi-smb-controller.yaml
curl -skSL "$repo/csi-smb-node.yaml" > csi-smb-node.yaml
curl -skSL "$repo/csi-smb-node-windows.yaml" > csi-smb-windows.yaml
curl -skSL "$repo/csi-smb-driver.yaml" > csi-smb-driver.yaml
curl -skSL "$repo/rbac-csi-smb.yaml" > rbac-csi-smb.yaml
