# Creating SMB CSI

- First take snapshot in proxmox

## Create a namespace

```
---
apiVersion: v1
kind: Namespace
metadata:
  name: csi-smb-provisioner
```

- push to gitlab

## Create secret

```
kubectl -n csi-smb-provisioner create secret generic smb-creds \
--from-literal username=kube \
--from-literal password=demo
```

## Deploy SMB CSI

- Download from k8s repo

- https://github.com/kubernetes-csi/csi-driver-smb/blob/master/docs/install-csi-driver-v1.15.0.md
  - https://raw.githubusercontent.com/kubernetes-csi/csi-driver-smb/v1.15.0/deploy/install-driver.sh

```
repo='https://raw.githubusercontent.com/kubernetes-csi/csi-driver-smb/master/deploy/v1.15.0/'

curl -skSL "$repo/csi-smb-controller.yaml" > csi-smb-controller.yaml
curl -skSL "$repo/csi-smb-node.yaml" > csi-smb-node.yaml
curl -skSL "$repo/csi-smb-driver.yaml" > csi-smb-driver.yaml
curl -skSL "$repo/rbac-csi-smb.yaml" > rbac-csi-smb.yaml
# curl -skSL "$repo/csi-smb-node-windows.yaml" > csi-smb-windows.yaml

# change the namespace
perl -i.bak -wnle 's/namespace: kube-system/namespace: csi-smb-provisioner/g; print' ./*.yaml
```


## Storage class

- Commit to gitlab
- storage class from: https://github.com/kubernetes-csi/csi-driver-smb/blob/master/deploy/example/e2e_usage.md
- Making it defualt: https://kubernetes.io/docs/tasks/administer-cluster/change-default-storage-class/

- important params:
  - __source__
  - annotation
  - namespace
  - parameters

```
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: smb
  namespace: csi-smb-provisioner
  annotations:
    storageclass.kubernetes.io/is-default-class: true
provisioner: smb.csi.k8s.io
parameters:
  source: # <your IP here> - //192.168.1.61/kubestore
  # if csi.storage.k8s.io/provisioner-secret is provided, will create a sub directory
  # with PV name under source
  csi.storage.k8s.io/provisioner-secret-name: smb-creds
  csi.storage.k8s.io/provisioner-secret-namespace: csi-smb-provisioner 
  csi.storage.k8s.io/node-stage-secret-name: smb-creds
  csi.storage.k8s.io/node-stage-secret-namespace: csi-smb-provisioner
reclaimPolicy: Delete  # available values: Delete, Retain
volumeBindingMode: Immediate
mountOptions:
  - dir_mode=0777
  - file_mode=0777
  - uid=1001
  - gid=1001
```


## Testing it

```
kubectl create -f https://raw.githubusercontent.com/kubernetes-csi/csi-driver-smb/master/deploy/example/statefulset.yaml
```

```
kubectl exec -it statefulset-smb-0 -- df -h
```
