#! /bin/bash

if ! $(git diff-index --diff-filter=A --quiet  -G '^ *kind\: *Secret' HEAD); then
	echo 'ERROR commit rejected because the following files contain kubernetes secrets:'
	echo
	git diff-index --name-only --diff-filter=A  -G '^ *kind\: *Secret' HEAD | perl -wnle 'print "  $_"' 
        echo
	echo "Consider converting secrets to bitnami SealedSecrets using:"
	echo
	git diff-index --name-only --diff-filter=A  -G '^ *kind\: *Secret' HEAD | perl -MFile::Basename -wnle 'print "  kubeseal -f $_ -w " . dirname($_) . "/sealed_secret_" . basename($_)'
        exit 1
fi
