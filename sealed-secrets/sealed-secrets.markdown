# Security

## Problem

```
kubectl create secret generic test-secret --from-literal=key=value -o yaml --dry-run=client  | tee test-secret.yaml
```

- secret from storage part
- show decoding secret

## Solution - Bitnami Sealed Secrets

- Explain sealed secrets - Asymetric crypto to encrypt secrets (from anywhere) and commit them to git for gitops that only the controller can decrypt
- https://github.com/bitnami-labs/sealed-secrets?tab=readme-ov-file#installation

### SealedSecrets controller (decryptor)

- Install the kubernetes controller which has a private key for decryption (unsealing) and creates secrets from SealedSecret types.
- create folder for sealed-secrets
- clone repo to your cluster and create folder in the repo

```
helm repo add sealed-secrets https://bitnami-labs.github.io/sealed-secrets
helm template sealed-secrets --include-crds -n kube-system --set-string fullnameOverride=sealed-secrets-controller sealed-secrets/sealed-secrets | tee seal-controller.yml
```

- `--key-renew-period=0` set to `0` in `Deployment.spec.template.spec.containers[0].args` for no renews required

- commit and push 

### `kubeseal` Client (encryptor)

- Only has public key so it can only Encrypt (seal) but not decrypt.
- Creates "SealedSecret" resources.

#### Basic (server) Use

- Add the following to `/etc/nixos/configuration.nix` and run `nixos-rebuild switch

```
  environment.systemPackages = [
    pkgs.kubeseal
  ];
```


- Test by pushing this to gitlab:

```
kubeseal --format=yaml --namespace default -f ./test-secret.yaml | tee sealed-test-secret.yaml
```

- note: namespace is required


#### Client Use

- Install with `nix-env -iA nixpkgs.kubeseal'
- Create `test-secret.yaml` on the client with `kubectl create secret generic test-secret --from-literal=key=value -o yaml --dry-run=client | tee test-secret.yaml`
- Public key Certificate should be committed to git
  - https://github.com/bitnami-labs/sealed-secrets#public-key--certificate
  - `kubeseal --fetch-cert | tee sealcert.yml`
  - SEALED_SECRETS_CERT


```
kubeseal --cert <file> --namespace default | tee mysecret.yaml
```

#### Client Use

#### Scopes

- `strict` is default - see github TODO
- set at CLI or annotations

These are the possible scopes:

  - __strict (default)__: the secret must be sealed with exactly the same name and namespace. These attributes become part of the encrypted data and thus changing name and/or namespace would lead to "decryption error".
  - __namespace-wide__: you can freely rename the sealed secret within a given namespace.
  - __cluster-wide__: the secret can be unsealed in any namespace and can be given any name.


## Pre-commit hooks

`vim .git/hooks/pre-commit`

- `chmod +x pre-commit`


```
if ! $(git diff-index --diff-filter=A --quiet  -G '^ *kind\: *Secret' HEAD); then
	echo 'ERROR commit rejected because the following files contain kubernetes secrets:'
	echo
	git diff-index --name-only --diff-filter=A  -G '^ *kind\: *Secret' HEAD | perl -wnle 'print "  $_"' 
        echo
	echo "Consider converting secrets to bitnami SealedSecrets using:"
	echo
	git diff-index --name-only --diff-filter=A  -G '^ *kind\: *Secret' HEAD | perl -MFile::Basename -wnle 'print "  kubeseal --namespace default  -f $_ -w " . dirname($_) . "/sealed_secret_" . basename($_)'
        exit 1
fi
```
