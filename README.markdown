# Kubernetes using Proxmox

## Post install Proxmox scripts

- https://helper-scripts.com/scripts

- Start

```s
bash -c "$(wget -qLO - https://github.com/tteck/Proxmox/raw/main/misc/post-pve-install.sh)"
```


- backups: https://pbs.proxmox.com/docs/pve-integration.html


## Design

- Turnkey file storage
  - Kubernetes SMB CSI
- ZFS

## NixOS k8s

- manual install

```
