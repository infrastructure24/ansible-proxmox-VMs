# agentk instructions



https://docs.gitlab.com/ee/user/clusters/agent/gitops/flux_tutorial.html

```
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install agentk gitlab/gitlab-agent \
    --namespace gitlab-agent-agentk \
    --create-namespace \
    --set image.tag=v17.1.0 \
    --set config.token=
    --set config.kasAddress=wss://kas.gitlab.com
```
