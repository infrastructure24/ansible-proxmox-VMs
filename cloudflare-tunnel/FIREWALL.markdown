## Part 2 - Cloudflare firewall rules 

### firewall rules 

- main page > website (mikeroservices.com) >  SSL/TLS > client certificates 
  - create cert
  - create mTLS rule (manage in WAF)
    - add IP from https://ipinfo.io/ip
    - add mTLS

### Install mTLS client cert

- Enable mTLS for a host: https://developers.cloudflare.com/ssl/client-certificates/enable-mtls/
- download CA cert: zero Trust ... settings ... Resources

- disable IP rule to test the curl command.

- This cURL command works with mTLS enabled (use from the try-1 directory):

```
$ curl  https://test.mikeroservices.com
$ curl --cert certificate.pem --key ./privatekey.pem https://test.mikeroservices.com | grep blocked
```

## Part 3 - External Client Setup

### Install Forward Proxy

- Show website is blocked

- Traffic goes `firefox ... forward proxy (localhost:8080) ... test.mikeroservices.com` the forward proxy handles the certs

- Install mitmproxy
  - download from - https://docs.mitmproxy.org/stable/overview-installation/
  - move to somewhere in your path or use locally

- start with `mitmweb`

- Proxy traffic in ubuntu's settings with `Network->Proxy`. Set http(s) to `http(s)://127.0.0.1` set port to `8080`

- Add mitmproxy certs
   - see - https://docs.mitmproxy.org/stable/concepts-certificates/
   - mitm.it
   - required for "valid CA rule"

- FIREFOX does not use ubuntu system certs - https://askubuntu.com/questions/244582/add-certificate-authorities-system-wide-on-firefox/248326#248326
  - Firefox certs are pem format
  - Firefox proxy settings
- adding certs docs: https://askubuntu.com/questions/73287/how-do-i-install-a-root-certificate/94861#94861

- cat certs: `cat ./certificate.pem ./privatekey.pem | tee mikeroservices.com.pem`
  - cert name must match website

- restart `mitmweb` proxy with:
  - on linux: `mitmweb --set client_certs=./mikeroservices.com.pem` 
  - on windows: `mitmweb --set client_certs=./mikeroservices.com.pem --mode local:firefox.exe`
    - https://mitmproxy.org/posts/local-redirect/windows/

- go to website

## On Linux

- make sure to add cert to linux
- `proxychains4 curl test.mikeroservices.com`
  - also kubectl...
  - `proxychains4 -f ./proxychains.conf  curl -k https://test.mikeroservices.com | grep blocked`
- `proxychains4 google-chrome`
- add cert to chrome
- for firefox - add cert, and add proxy settings to firefox


## Other

- zero trust / access / add IdP / google 
  - Decided to not use this because it costs $ for more than 50 users

- TODO: show testing WAF rules

- mTLS with kubectl
