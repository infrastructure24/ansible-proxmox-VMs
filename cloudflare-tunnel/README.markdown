## Part 1 - tunnel to basic site

### Setup Basic Site

- Create Deployment:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: httpbin-deployment
  namespace: default
spec:
  selector:
    matchLabels:
      app: httpbin
  replicas: 2
  template:
    metadata:
      labels:
        app: httpbin
    spec:
      containers:
        - name: httpbin
          image: kennethreitz/httpbin:latest
          ports:
            - containerPort: 80
---
apiVersion: v1
kind: Service
metadata:
  name: web-service
  namespace: default
spec:
  selector:
    app: httpbin
  ports:
    - protocol: TCP
      port: 80
```

- curl it to check that it's up

### Cloudflare Tunnel

- Create a tunnel on cloudflare.com at `zero trust/Networks/Tunnels`
  - create tunnel
  - cloudflared
  - name it
  - copy token 

#### Sealed Secret from Token

```
kubectl create secret generic cloudflare-token --namespace default --from-literal=CLOUDFLARE_TOKEN=  TOKEN_HERE     -o yaml --dry-run=client | kubeseal --format=yaml --namespace default | tee sealed-cloudflare-token.yaml
```


#### Deployment using secret

- https://stackoverflow.com/questions/50248525/is-there-a-way-to-put-kubernetes-secret-value-in-args-field-of-yaml-file

```
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: cloudflared
  name: cloudflared-deployment
  namespace: default
spec:
  replicas: 2
  selector:
    matchLabels:
      pod: cloudflared
  template:
    metadata:
      creationTimestamp: null
      labels:
        pod: cloudflared
    spec:
      containers:
        - command:
            - cloudflared
            - tunnel
            - --no-autoupdate
            # In a k8s environment, the metrics server needs to listen outside the pod it runs on.
            - --metrics 
            - 0.0.0.0:2000
            # The address 0.0.0.0:2000 allows any pod in the namespace.
            - run
          args:
            - --token
            - "$(CLOUDFLARE_TOKEN)"
          env:
            - name: CLOUDFLARE_TOKEN
              valueFrom:
                secretKeyRef:
                  name: cloudflare-token  
                  key: CLOUDFLARE_TOKEN
          image: cloudflare/cloudflared:latest
          name: cloudflared
          livenessProbe:
            httpGet:
              # Cloudflared has a /ready endpoint which returns 200 if and only if
              # it has an active connection to the edge.
              path: /ready
              port: 2000
            failureThreshold: 1
            initialDelaySeconds: 10
            periodSeconds: 10
```


- https://developers.cloudflare.com/cloudflare-one/connections/connect-networks/deploy-tunnels/deployment-guides/kubernetes/
- https://github.com/adyanth/cloudflare-operator

#### Route traffic to web-service

- In cloudflare route traffic to the service IP found with `kubectl describe svc web-service`
- You may need to delete an existing DNS name if there is one

## Part 2 -  google auth

- https://developers.cloudflare.com/cloudflare-one/identity/idp-integration/google/
- https://developers.google.com/identity/openid-connect/openid-connect
- https://developers.google.com/identity/protocols/oauth2
- https://developers.cloudflare.com/cloudflare-one/policies/access/

- Make sure there are no WAF rules under the website's security section
- zero trust/access -> Bypass rule for IP from my IP
- Add an IdP rule

## Part 3 - tunnel to kubectl

- https://github.com/cloudflare/argo-tunnel-examples/tree/master/named-tunnel-k8s
- https://www.reddit.com/r/kubernetes/comments/13ztbik/ditching_ingressnginx_for_cloudflare_tunnels/
  - you can also tunnel the control plane to kubectl via Cloudflare Access authz
- Also add a cloudflare security rules for blocking traffic from non-logged in people
- https://blog.cloudflare.com/releasing-kubectl-support-in-access/

## Part 4 - Delete Resources

- re-cretate kubeconfig
- delete SSO creds
- blur e-mail etc.
- delete tunnel
- delete k8s cloudflared service, token and tunnel
