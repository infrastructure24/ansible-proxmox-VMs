# mTLS Client Setup - Linux

- Note on linux newer versions of mitmproxy (11.1 and up) can intercept application traffic when run with sudo (like the previous windows tutorial).

## Install Forward Proxy (mitmproxy)

- Show test.mikeroservices.com is blocked

- Traffic goes `webbrowser (proxychains) ... forward proxy (localhost:8080) ... test.mikeroservices.com` the forward proxy handles the certs

- Install download mitmproxy from mitmproxy.org and put in path

### Install LibreWolf

- https://librewolf.net/installation/debian/

```
sudo apt update && sudo apt install extrepo -y
sudo extrepo enable librewolf
sudo apt update && sudo apt install librewolf -y
```

### Install Proxychains

- proxychains overloads `LD_PRELOAD`
- install steps

```
sudo apt install proxychains-ng
sudo vim ./proxychains.conf
```

- add a file named `proxychains.conf` with the following content

```
proxy_dns
strict_chain
[ProxyList]
http 127.0.0.1 8080 
```

### Set up mitmproxy certs

- run `mitmweb`
- show google is blocked with `proxychains4 -f ./proxychains.conf librewolf google.com`
  - go to `mitm.it` and add mitmproxy certs with linux instructions
- logout and login

- key pinning issue: https://librewolf.net/docs/faq/#im-getting-mozilla_pkix_error_key_pinning_failure-what-can-i-do
  - We enable strict Public Key Pinning, so user level MiTM is not supported by default: this is problematic when an antivirus is trying to monitor your network traffic by using its own certificates.
  - If you use this kind of antivirus software and you want to disable this, you need to change security.cert_pinning.enforcement_level to 1 in about:config.

- Import mitm CA cert to librewolf (can be done with `libnss3-tools`)

## Setup mTLS cert

- Combine the cloudflare certs into file named after the website ('mikeroservices.com.pem') `cat ./certificate.pem ./pk.pem | tee mikeroservices.com.pem`

## Run mitmproxy and connect with mTLS

- start with `mitmweb --set client_certs=./mikeroservices.com.pem`
- `proxychains -f ./proxychains.conf librewolf https://test.mikeroservices.com`
- can also do `proxychains -f ./proxychains.conf librewolf bash`


## (alternative) Edit firefox settings

- add CA, and add proxy?
  - https://librewolf.net/installation/debian/
- CERT script: https://superuser.com/questions/1717914/make-chrome-trust-the-linux-system-certificate-store-or-select-certificates-via
