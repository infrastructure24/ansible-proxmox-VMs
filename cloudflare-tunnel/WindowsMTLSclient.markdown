# mTLS Client Setup - Windows

## Install Forward Proxy (mitmproxy)

- Show website is blocked

- Traffic goes `webbroser ... forward proxy (localhost:8080) ... test.mikeroservices.com` the forward proxy handles the certs

- Install mitmproxy
  - download from - https://docs.mitmproxy.org/stable/overview-installation/
  - move to somewhere in your path or use locally

### Set up mitmproxy certs

- run mitmweb as admin
- google is blocked
- Add mitmproxy certs from `mitm.it`
- logout and login

## Setup mTLS cert

- Have certs downloaded from cloudflare
- Combine certs into file named after the website ('mikeroservices.com.pem')
  - run `notepad mikeroservices.com.pem` and copy output of `get-content ./certificate.pem,./pk.pem` to it
  

## Run mitmproxy

- start with `mitmweb --set client_certs=./mikeroservices.com.pem`

- proxy for `msedge.exe`

- on windows: `mitmweb --set client_certs=./mikeroservices.com.pem --mode regular,local:msedge.exe`

- go to `https://test.mikeroservices.com`
